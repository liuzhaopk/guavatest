package cn.com.franke.collections;

import com.google.common.collect.*;
import org.junit.Test;

/**
 * Created by liuzh on 2017/4/29.
 */
public class OtherTest {
//    　　ClassToInstanceMap
//　　有的时候，你的map的key并不是一种类型，他们是很多类型，你想通过映射他们得到这种类型，guava提供了ClassToInstanceMap满足了这个目的。
//            　　除了继承自Map接口，ClassToInstaceMap提供了方法 T getInstance(Class<T>) 和 T putInstance(Class<T>, T),消除了强制类型转换。
//            　　该类有一个简单类型的参数，通常称为B，代表了map控制的上层绑定，例如：
//    ClassToInstanceMap<Number> numberDefaults = MutableClassToInstanceMap.create();
//numberDefaults.putInstance(Integer.class, Integer.valueOf(0));
//
//　　从技术上来说，ClassToInstanceMap<B> 实现了Map<Class<? extends B>, B>，或者说，这是一个从B的子类到B对象的映射，这可能使得ClassToInstanceMap的泛型轻度混乱，但是只要记住B总是Map的上层绑定类型，通常来说B只是一个对象。
//            　　guava提供了有用的实现， MutableClassToInstanceMap 和 ImmutableClassToInstanceMap.
//　　重点：像其他的Map<Class,Object>,ClassToInstanceMap 含有的原生类型的项目，一个原生类型和他的相应的包装类可以映射到不同的值；

    @Test
    public void ClassToInstanceMapTest() {
        ClassToInstanceMap<String> classToInstanceMapString = MutableClassToInstanceMap.create();
        ClassToInstanceMap<Person> classToInstanceMap = MutableClassToInstanceMap.create();
        Person person = new Person("peida", 20);
        System.out.println("person name :" + person.name + " age:" + person.age);
        classToInstanceMapString.put(String.class, "peida");
        System.out.println("string:" + classToInstanceMapString.getInstance(String.class));

        classToInstanceMap.putInstance(Person.class, person);
        Person person1 = classToInstanceMap.getInstance(Person.class);
        System.out.println("person1 name :" + person1.name + " age:" + person1.age);
        Person person2 = new Person("jetty", 21);
        classToInstanceMap.putInstance(Person.class, person2);
        System.out.println(classToInstanceMap.getInstance(Person.class));
    }

    //　　RangeSet
//　　RangeSet用来处理一系列不连续，非空的range。当添加一个range到一个RangeSet之后，任何有连续的range将被自动合并，而空的range将被自动去除。例如：
    @Test
    public void RangeSetTest() {
        RangeSet<Integer> rangeSet = TreeRangeSet.create();
        rangeSet.add(Range.closed(1, 10));
        System.out.println("rangeSet:" + rangeSet);
        rangeSet.add(Range.closedOpen(11, 15));
        System.out.println("rangeSet:" + rangeSet);
        rangeSet.add(Range.open(15, 20));
        System.out.println("rangeSet:" + rangeSet);
        rangeSet.add(Range.openClosed(0, 0));
        System.out.println("rangeSet:" + rangeSet);
        rangeSet.remove(Range.open(5, 10));
        System.out.println("rangeSet:" + rangeSet);
        Range.closed(1, 10).canonical(DiscreteDomain.integers());

    }

//       RangeMap
//　　RangeMap代表了非连续非空的range对应的集合。不像RangeSet，RangeMap不会合并相邻的映射，甚至相邻的range对应的是相同的值。例如：
    @Test
    public void RangeMapTest() {
        RangeMap<Integer, String> rangeMap = TreeRangeMap.create();
        rangeMap.put(Range.closed(1, 10), "foo");
        System.out.println("rangeMap:" + rangeMap);
        rangeMap.put(Range.open(3, 6), "bar");
        System.out.println("rangeMap:" + rangeMap);
        rangeMap.put(Range.open(10, 20), "foo");
        System.out.println("rangeMap:" + rangeMap);
        rangeMap.remove(Range.closed(5, 11));
        System.out.println("rangeMap:" + rangeMap);
    }

//    输出：
//    rangeMap:[[1‥10]=foo]
//    rangeMap:[[1‥3]=foo, (3‥6)=bar, [6‥10]=foo]
//    rangeMap:[[1‥3]=foo, (3‥6)=bar, [6‥10]=foo, (10‥20)=foo]
//    rangeMap:[[1‥3]=foo, (3‥5)=bar, (11‥20)=foo]
//
//            　　RangeMap的视图
//　　RangeMap提供了两种视图：
//            　　asMapOfRanges():返回Map<Range<K>, V>类型的视图。这个操作可以被用作迭代操作。
//            　　subRangeMap(Range<K>)提供给定Range的交集。这个操作可以推广到传统的headMap, subMap, 和tailMap。
class Person {
    public String name;
    public int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

}


