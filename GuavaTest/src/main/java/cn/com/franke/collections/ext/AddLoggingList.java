package cn.com.franke.collections.ext;

import com.google.common.collect.ForwardingList;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.List;

/**
 * Created by liuzh on 2017/5/1.
 */
public class AddLoggingList<E> extends ForwardingList<E> {
    final List<E> delegate = Lists.newArrayList();

    // 你可以覆盖这个方法来返回被装饰对象
    @Override
    protected List<E> delegate() {
        return delegate;
    }

    @Override
    public void add(int index, E elem) {
        log(index, elem);
        super.add(index, elem);
    }

    @Override
    public boolean add(E elem) {
        return standardAdd(elem); // 用add(int, E)实现
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return standardAddAll(c); // 用add实现
    }

    private void log(int index, E elem) {
        System.out.println(String.format("index:%s,elem:%s", index, elem.toString()));
    }

    public static <E> AddLoggingList<E> create() {
        return new AddLoggingList();
    }
}
