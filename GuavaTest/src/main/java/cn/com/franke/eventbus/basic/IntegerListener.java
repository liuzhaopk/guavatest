package cn.com.franke.eventbus.basic;

import com.google.common.eventbus.Subscribe;

/**
 * Created by liuxianzhao on 2017/5/4.
 */
public class IntegerListener {
    private Integer lastMessage;

    @Subscribe
    public void listen(Integer integer) {
        lastMessage = integer;
        System.out.println("Message:"+lastMessage);
    }

    public Integer getLastMessage() {
        return lastMessage;
    }
}
