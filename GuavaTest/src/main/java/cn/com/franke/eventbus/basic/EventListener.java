package cn.com.franke.eventbus.basic;

import com.google.common.eventbus.Subscribe;

/**
 * 消息接受类：
 * Created by liuxianzhao on 2017/5/4.
 */
public class EventListener {
    public int lastMessage = 0;

    @Subscribe
    public void listen(TestEvent event) {
        lastMessage = event.getMessage();
        System.out.println("EventListener.listen Message:"+lastMessage);
    }

    public int getLastMessage() {
        return lastMessage;
    }
}
