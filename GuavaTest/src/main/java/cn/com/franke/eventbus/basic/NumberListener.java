package cn.com.franke.eventbus.basic;

import com.google.common.eventbus.Subscribe;

/**
 * Created by liuxianzhao on 2017/5/4.
 */
public class NumberListener {
    private Number lastMessage;

    @Subscribe
    public void listen(Number integer) {
        lastMessage = integer;
        System.out.println("Message:"+lastMessage);
    }

    public Number getLastMessage() {
        return lastMessage;
    }
}
