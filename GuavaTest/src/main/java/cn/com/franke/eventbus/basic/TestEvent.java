package cn.com.franke.eventbus.basic;

/**
 * 消息封装类：
 * Created by liuxianzhao on 2017/5/4.
 */
public class TestEvent {
    private final int message;
    public TestEvent(int message) {
        this.message = message;
        System.out.println("TestEvent event message:"+message);
    }
    public int getMessage() {
        return message;
    }
}
