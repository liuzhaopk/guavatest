package cn.com.franke.cache;

import com.google.common.cache.*;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * Created by liuzh on 2017/5/1.
 */
public class GuavaCacheTest {

    @Test
    public void TestLoadingCache() throws Exception {
        LoadingCache<String, String> cahceBuilder = CacheBuilder
                .newBuilder()
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        String strProValue = "hello " + key + "!";
                        return strProValue;
                    }
                });
        System.out.println("jerry value:" + cahceBuilder.get("jerry"));
        System.out.println("peida value:" + cahceBuilder.get("peida"));
        cahceBuilder.put("harry", "ssdded");
        System.out.println("harry value:" + cahceBuilder.get("harry"));
    }

    @Test
    public void testcallableCache() throws Exception {
        Cache<String, String> cache = CacheBuilder.newBuilder().maximumSize(1000).build();
        System.out.println("jerry value : " + cache.getIfPresent("jerry"));
        String resultVal = cache.get("jerry", new Callable<String>() {
            public String call() {
                String strProValue = "hello " + "jerry" + "!";
                return strProValue;
            }
        });
        System.out.println("jerry value : " + resultVal);
        System.out.println("jerry value : " + cache.getIfPresent("jerry"));
    }

    @Test
    public void testCache() throws Exception {
        LoadingCache<String, String> commonCache = commonCache("peida");
        System.out.println("peida:" + commonCache.get("peida"));
        commonCache.apply("harry");
        System.out.println("harry:" + commonCache.get("harry"));
        commonCache.apply("lisa");
        System.out.println("lisa:" + commonCache.get("lisa"));
    }
//    cache的参数说明：
//            　　回收的参数：
//            　　1. 大小的设置：CacheBuilder.maximumSize(long)  CacheBuilder.weigher(Weigher)  CacheBuilder.maxumumWeigher(long)
//            　　2. 时间：expireAfterAccess(long, TimeUnit) expireAfterWrite(long, TimeUnit)
//            　　3. 引用：CacheBuilder.weakKeys() 使用弱引用存储键。当键没有其它（强或软）引用时，缓存项可以被垃圾回收。因为垃圾回收仅依赖恒等式（==），使用弱引用键的缓存用==而不是equals比较键。
//                              CacheBuilder.weakValues() 使用弱引用存储值。当值没有其它（强或软）引用时，缓存项可以被垃圾回收。因为垃圾回收仅依赖恒等式（==），使用弱引用值的缓存用==而不是equals比较值。
//                              CacheBuilder.softValues() 使用软引用存储值。软引用只有在响应内存需要时，才按照全局最近最少使用的顺序回收。考虑到使用软引用的性能影响，我们通常建议使用更有性能预测性的缓存大小限定（见上文，基于容量回收）。使用软引用值的缓存同样用==而不是equals比较值。
//            　　4. 明确的删除：invalidate(key)  invalidateAll(keys)  invalidateAll()
//　　            5. 删除监听器：CacheBuilder.removalListener(RemovalListener)
//            　　
//
//            　　refresh机制：
//            　　1. LoadingCache.refresh(K)  在生成新的value的时候，旧的value依然会被使用。
//            　　2. CacheLoader.reload(K, V) 生成新的value过程中允许使用旧的value
//　　            3. CacheBuilder.refreshAfterWrite(long, TimeUnit) 自动刷新cache

    /**
     * 不需要延迟处理(泛型的方式封装)
     *
     * @return
     */
    public <K, V> LoadingCache<K, V> cached(CacheLoader<K, V> cacheLoader) {
        LoadingCache<K, V> cache = CacheBuilder
                .newBuilder()
                .maximumSize(2)
                .weakKeys()//使用弱引用存储键。当键没有其它（强或软）引用时，缓存项可以被垃圾回收。因为垃圾回收仅依赖恒等式（==），使用弱引用键的缓存用==而不是equals比较键。
                .softValues()
                .refreshAfterWrite(120, TimeUnit.SECONDS)
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .removalListener(new RemovalListener<K, V>() {
                    @Override
                    public void onRemoval(RemovalNotification<K, V> rn) {
                        System.out.println(rn.getKey() + "被移除");

                    }
                })
                .build(cacheLoader);
        return cache;
    }

    /**
     * 通过key获取value
     * 调用方式 commonCache.get(key) ; return String
     *
     * @param key
     * @return
     * @throws Exception
     */

    public LoadingCache<String, String> commonCache(final String key) throws Exception {
        LoadingCache<String, String> commonCache = cached(new CacheLoader<String, String>() {
            @Override
            public String load(String key) throws Exception {
                return "hello " + key + "!";
            }
        });
        return commonCache;
    }

    private Cache<String, String> cacheFormCallable;

    @Test
    public void testCallableCache() throws Exception {
        final String u1name = "peida";
        final String u2name = "jerry";
        final String u3name = "lisa";
        cacheFormCallable = callableCached();
        System.out.println("peida:" + getCallableCache(u1name));
        System.out.println("jerry:" + getCallableCache(u2name));
        System.out.println("lisa:" + getCallableCache(u3name));
        System.out.println("peida:" + getCallableCache(u1name));//　说明：Callable只有在缓存值不存在时，才会调用，比如第二次调用getCallableCache(u1name)直接返回缓存中的值

    }

    /**
     * 对需要延迟处理的可以采用这个机制；(泛型的方式封装)
     *
     * @param <K>
     * @param <V>
     * @return V
     * @throws Exception
     */
    public static <K, V> Cache<K, V> callableCached() throws Exception {
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        Cache<K, V> cache = CacheBuilder
                .newBuilder()
                .maximumSize(2)
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .removalListener(RemovalListeners.asynchronous(new RemovalListener<K, V>() {
                    @Override
                    public void onRemoval(RemovalNotification<K, V> rn) {
                        System.out.println(rn.getKey() + "被移除");

                    }
                }, threadPool))
                .build();
        return cache;
    }


    private String getCallableCache(final String userName) {
        try {
            //Callable只有在缓存值不存在时，才会调用
            return cacheFormCallable.get(userName, new Callable<String>() {
                @Override
                public String call() throws Exception {
                    System.out.println(userName + " from db");
                    return "hello " + userName + "!";
                }
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

}
