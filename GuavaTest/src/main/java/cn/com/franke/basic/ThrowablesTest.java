package cn.com.franke.basic;

import com.google.common.base.Throwables;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by liuxianzhao on 2017/4/27.
 */
public class ThrowablesTest {
    @Test
    public void testThrowables() {
        try {
            throw new Exception("自定义异常");
        } catch (Throwable t) {
            String ss = Throwables.getStackTraceAsString(t);
            System.out.println("ss:" + ss);
            Throwables.propagate(t);
        }
    }

    @Test
    public void testThrowIfUnchecked() {
        call();
    }

    public void call() {
        try {
            if (1 / 0 == 1){
                System.out.println("woqu ");
            }
            throw new IOException("哈哈哈哈");
        } catch (Exception t) {
//            Throwables.throwIfInstanceOf(t, IOException.class);
            System.out.println("到这了........");
            Throwables.throwIfUnchecked(t);
        }
    }

    /*将检查异常转换成未检查异常*/
    @Test
    public void testCheckException() {
        try {
            URL url = new URL("http://ociweb.com");
            final InputStream in = url.openStream();
            // read from the input stream
            in.close();
        } catch (Throwable t) {
            throw Throwables.propagate(t);
        }
    }
//    　传递异常的常用方法：
//      1.RuntimeException propagate(Throwable)：把throwable包装成RuntimeException，用该方法保证异常传递，抛出一个RuntimeException异常
//　　2.void throwIfInstanceOf(Throwable, Class<X extends Exception>) throws X：当且仅当它是一个X的实例时，传递throwable
//　　3.void propagateIfPossible(Throwable)：当且仅当它是一个RuntimeException和Error时，传递throwable
//　　4.void propagateIfPossible(Throwable, Class<X extends Throwable>) throws X：当且仅当它是一个RuntimeException和Error时，或者是一个X的实例时，传递throwable。

    public Void testPropagateIfPossible() throws Exception {
        try {
            throw new Exception();
        } catch (Throwable t) {
            Throwables.propagateIfPossible(t, Exception.class);
            Throwables.propagate(t);
        }

        return null;
    }
//    Guava的异常链处理方法：
//      1.Throwable getRootCause(Throwable)
//　　2.List<Throwable> getCausalChain(Throwable)
//　　3.String getStackTraceAsString(Throwable)

}
