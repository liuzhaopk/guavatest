package cn.com.franke.io;

import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import org.junit.Test;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by liuxianzhao on 2017/5/3.
 */
public class IOStreamTest {

    @Test
    public void charStreamTest() {
        try {
            // String toString(Readable)
            String path = URLDecoder.decode(this.getClass().getResource("/doc/13.guava中定义的String操作.txt").getFile(), "utf8");
            FileReader fr = new FileReader(path);
            System.out.println(CharStreams.toString(fr));
            fr.close();

            //List<String> readLines(Readable)
            FileReader fr1 = new FileReader(path);
            List<String> list = CharStreams.readLines(fr1);
            list.forEach(System.out::println);
            fr1.close();

            //copy(Readable, Appendable)
            FileReader fr2 = new FileReader(path);
            FileWriter fw = new FileWriter("D://test//11.txt");
            CharStreams.copy(fr2, fw);
            fw.flush();
            fw.close();
            fr2.close();

            //void skipFully(Reader, long)
            FileReader fr3 = new FileReader(path);
            CharStreams.skipFully(fr3, 10);
            System.out.println(CharStreams.toString(fr3));
            fr3.close();

            Writer nullWriter = CharStreams.nullWriter();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void byteStreamTest() {
        try {
            //byte[] toByteArray(InputStream)
            InputStream is = this.getClass().getResourceAsStream("/doc/13.guava中定义的String操作.txt");
            byte[] byteArr = ByteStreams.toByteArray(is);
//            byte[] byteArr = IOUtils.toByteArray(is);
            is.close();
            //long copy(InputStream, OutputStream)
            InputStream is1 = this.getClass().getResourceAsStream("/doc/13.guava中定义的String操作.txt");
            FileOutputStream fos = new FileOutputStream("D://test//1.txt");
            ByteStreams.copy(is1, fos);
            fos.flush();
            fos.close();
            is1.close();
            // void readFully(InputStream, byte[])
            InputStream is2 = this.getClass().getResourceAsStream("/doc/13.guava中定义的String操作.txt");
            byte[] byteArr2 = new byte[is2.available() > 8096 ? 8096 : is2.available()];
            ByteStreams.readFully(is2, byteArr2);
//            IOUtils.readFully(is2,byteArr2);
            is2.close();
            // void skipFully(InputStream, long)
            InputStream is3 = this.getClass().getResourceAsStream("/doc/13.guava中定义的String操作.txt");
            FileOutputStream fos3 = new FileOutputStream("D://test//3.txt");
            ByteStreams.skipFully(is3, 1024);
            ByteStreams.copy(is3, fos3);
            fos3.flush();
            fos3.close();
            is3.close();
            OutputStream nullos = ByteStreams.nullOutputStream();
            nullos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
