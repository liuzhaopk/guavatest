package cn.com.franke.io;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multiset;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;

/**
 * Created by liuxianzhao on 2017/5/4.
 */
public class SourceSinkTest {
    //                字节	     字符
//    读	ByteSource	CharSource
//    写	ByteSink	    CharSink
    @Test
    public void test1() {
        try {
            URL url = this.getClass().getResource("/test/test1.txt");
            String filePath = URLDecoder.decode(this.getClass().getResource("/test/test2.txt").getFile(), "UTF-8");
            String filePath1 = URLDecoder.decode(this.getClass().getResource("/test/test3.txt").getFile(), "UTF-8");

            File file1 = new File(filePath);
            ImmutableList<String> lines = Files.asCharSource(file1, Charsets.UTF_8).readLines();//获得一个源，并进行读写，返回一个列表
            lines.forEach(System.out::println);

            Multiset<String> wordOccurrences = HashMultiset.create(
                    Splitter.on(CharMatcher.whitespace())//通过空格拆分
                            .trimResults()
                            .omitEmptyStrings()
                            .split(Files.asCharSource(file1, Charsets.UTF_8).read()));
            wordOccurrences.forEach(System.out::println);//打印统计结果

            //SHA-1 a file
            HashCode hash = Files.asByteSource(file1).hash(Hashing.sha1());
            System.out.println(hash);
            //Copy the data from a URL to a file
            File file2 = new File(filePath1);
            Resources.asByteSource(url).copyTo(Files.asByteSink(file2));//copy文件

//            createParentDirs(File)	必要时为文件创建父目录
//            getFileExtension(String)	返回给定路径所表示文件的扩展名
//            getNameWithoutExtension(String)	返回去除了扩展名的文件名
//            simplifyPath(String)	规范文件路径，并不总是与文件系统一致，请仔细测试
//            fileTreeTraverser()	返回TreeTraverser用于遍历文件树

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
