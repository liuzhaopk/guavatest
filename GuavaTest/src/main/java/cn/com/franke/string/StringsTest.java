package cn.com.franke.string;

import com.google.common.base.*;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by liuzh on 2017/5/1.
 */
public class StringsTest {
    @Test
    public void test1() {
//        1. 使用com.google.common.base.Strings类的isNullOrEmpty(input)方法判断字符串是否为空

        //Strings.isNullOrEmpty(input) demo
        String input = "";
        boolean isNullOrEmpty = Strings.isNullOrEmpty(input);
        System.out.println("input " + (isNullOrEmpty ? "is" : "is not") + " null or empty.");
//        2. 获得两个字符串相同的前缀或者后缀

        //Strings.commonPrefix(a,b) demo
        String a = "com.jd.coo.Hello";
        String b = "com.jd.coo.Hi";
        String ourCommonPrefix = Strings.commonPrefix(a, b);
        System.out.println("a,b common prefix is " + ourCommonPrefix);

        //Strings.commonSuffix(a,b) demo
        String c = "com.google.Hello";
        String d = "com.jd.Hello";
        String ourSuffix = Strings.commonSuffix(c, d);
        System.out.println("c,d common suffix is " + ourSuffix);
//        3. Strings的padStart和padEnd方法来补全字符串

        int minLength = 4;
        String padEndResult = Strings.padEnd("123", minLength, '0');
        System.out.println("padEndResult is " + padEndResult);

        String padStartResult = Strings.padStart("1", 2, '0');
        System.out.println("padStartResult is " + padStartResult);
//        4. 使用Splitter类来拆分字符串
//        Splitter类可以方便的根据正则表达式来拆分字符串，可以去掉拆分结果中的空串，可以对拆分后的字串做trim操作，还可以做二次拆分。
//        我们先看一个基本的拆分例子：

        Iterable<String> splitResults = Splitter.onPattern("[,，]{1,}")
                .trimResults()//trim结果
                .omitEmptyStrings()//忽略空字符串
                .split("hello,word, ,  ,世界，水平");
        splitResults.forEach(System.out::println);
//        Splitter的onPattern方法传入的是一个正则表达式，其后紧跟的trimResults()方法表示要对结果做trim，omitEmptyStrings()表示忽略空字符串，split方法会执行拆分操作。
//
//        split返回的结果为Iterable<String>，我们可以使用for循环语句来逐个打印拆分字符串的结果。
//
//        Splitter还有更强大的功能，做二次拆分，这里二次拆分的意思是拆分两次，例如我们可以将a=b;c=d这样的字符串拆分成一个Map<String,String>。

        String toSplitString = "a=b;c=d,e=f";
        Map<String, String> kvs = Splitter.onPattern("[,;]{1,}").withKeyValueSeparator('=').split(toSplitString);
        for (Map.Entry<String, String> entry : kvs.entrySet()) {
            System.out.println(String.format("%s=%s", entry.getKey(), entry.getValue()));
        }
//        二次拆分首先是使用onPattern做第一次的拆分，然后再通过withKeyValueSeperator('')方法做第二次的拆分。
        String spiltStr = "1|2|3|4||6|7|8|9||";
        List<String> list = Splitter.onPattern("\\|").splitToList(spiltStr);
        list.forEach(System.out::println);
//        5. 有拆分字符串必然就有合并字符串，guava为我们提供了Joiner类来做字符串的合并
//
//        我们先看一个简单的示例：

        String joinResult = Joiner.on(" ").join(new String[]{"hello", "world"});
        System.out.println(joinResult);
//        上面例子中我们使用Joiner.on(" ").join(xx)来合并字符串。很简单也很有效。
//
//        Splitter方法可以对字符串做二次的拆分，对应的Joiner也可以逆向操作，将Map<String,String>做合并。我们看下下面的例子：

        Map<String, String> map = new HashMap<String, String>();
        map.put("a", "b");
        map.put("c", "d");
        String mapJoinResult = Joiner.on(",").withKeyValueSeparator("=").join(kvs);
        System.out.println(mapJoinResult);
//        使用withKeyValueSeparator方法可以对map做合并。合并的结果是:a=b,c=d
//
//        guava库中还可以对字符串做大小写转换（CaseFormat枚举），可以对字符串做模式匹配。使用起来都很方便，就不一一介绍了。
    }

    @Test
    public void test2() {
//        字符匹配器[CharMatcher]
        String string = "1abcdefg   h  \n   \t........1234";
        String noControl = CharMatcher.javaIsoControl().removeFrom(string); //移除control字符
        String theDigits = CharMatcher.digit().retainFrom(string); //只保留数字字符
        String spaced = CharMatcher.whitespace().trimAndCollapseFrom(string, ' ');//去除两端的空格，并把中间的连续空格替换成单个空格
        String noDigits = CharMatcher.javaDigit().replaceFrom(string, "*"); //用*号替换所有数字
        String lowerAndDigit = CharMatcher.javaDigit().or(CharMatcher.javaLowerCase()).retainFrom(string);// 只保留数字和小写字母
//        collapseFrom(CharSequence,   char)	把每组连续的匹配字符替换为特定字符。如WHITESPACE.collapseFrom(string, ‘ ‘)把字符串中的连续空白字符替换为单个空格。
//        matchesAllOf(CharSequence)	测试是否字符序列中的所有字符都匹配。
//        removeFrom(CharSequence)	从字符序列中移除所有匹配字符。
//        retainFrom(CharSequence)	在字符序列中保留匹配字符，移除其他字符。
//        trimFrom(CharSequence)	移除字符序列的前导匹配字符和尾部匹配字符。
//        replaceFrom(CharSequence,   CharSequence)	用特定字符序列替代匹配字符。
//        anyOf(CharSequence)	枚举匹配字符。如CharMatcher.anyOf(“aeiou”)匹配小写英语元音
//        is(char)	给定单一字符匹配。
//        inRange(char, char)	给定字符范围匹配，如CharMatcher.inRange(‘a’, ‘z’)


        byte[] bytes = string.getBytes(Charsets.UTF_8);
//        大小写格式[CaseFormat]
        String origin = "CONSTANT_NAME";
        System.out.println(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, origin)); // returns "constantName"
        System.out.println(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, origin));//ConstantName

        CaseFormat fromFormat = CaseFormat.LOWER_CAMEL;
        CaseFormat toFormat = CaseFormat.UPPER_CAMEL;
        String s = "lowerCamel";
        System.out.println(fromFormat.to(toFormat, s));
    }
}
