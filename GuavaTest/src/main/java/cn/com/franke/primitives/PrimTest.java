package cn.com.franke.primitives;

import com.google.common.base.Joiner;
import com.google.common.base.Throwables;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import org.junit.Test;

import java.util.*;
import java.util.function.ToIntFunction;

/**
 * Created by liuzh on 2017/5/2.
 */
public class PrimTest {
    @Test
    public void test1() {
//        Arrays.asList
        String[] str = {"liu", "xian", "zhao"};
        List<String> list = Arrays.asList(str);
        list.forEach(System.out::println);
//        Collection.toArray()
        System.out.println(list.toArray());
//        Iterables.concat
        Set<String> set = Sets.newHashSet("1", "2", "3");
        Iterable<String> iterable = Iterables.concat(list, set);
        iterable.forEach(System.out::println);
//        Collection.contains
        System.out.println(list.contains("liu"));
        System.out.println(list.contains("1"));
//        List.indexOf
        System.out.println(list.indexOf("liu"));
//        List.lastIndexOf
        System.out.println(list.lastIndexOf("liu"));
//        Collections.min
        System.out.println(Collections.min(list));
//        Collections.max
        System.out.println(Collections.max(list));
//        Joiner.on(separator).join
        System.out.println(Joiner.on("#").join(str));
//        Ordering.natural().lexicographical()
        Ordering order = Ordering.natural().lexicographical();
    }

    @Test
    public void test2() {
//        方法签名 描述 类似方法 可用性
//        List<Wrapper> asList (prim…backingArray)把数组转为相应包装类的List Arrays.asList 符号无关*
        int[] intArr = {1, 2, 3, 4, 5};
        List<Integer> list = Ints.asList(intArr);
        list.forEach(System.out::println);

//        prim[] toArray (Collection < Wrapper > collection) 把集合拷贝为数组，和collection.toArray() 一样线程安全 Collection.toArray() 符号无关
        Integer[] intArr_1 = new Integer[4];
        Integer[] intArr_2 = list.toArray(intArr_1);//he array into which the elements of this list are to be stored, if it is big enough; otherwise, a new array of thesame runtime type

        try {
            for (int a : intArr_1) {
                System.out.println("intArr_1:" + a);
            }
        } catch (Exception e) {
            System.out.println(Throwables.getStackTraceAsString(e));
        }
        for (int b : intArr_2) {
            System.out.println("intArr_2:" + b);
        }
//        prim[] concat (prim[]…arrays)串联多个原生类型数组 Iterables.concat 符号无关
        int[] intArr_3 = {1, 2, 3, 4, 5};
//        int[] intArr_4 = Ints.concat(intArr, intArr_3);
        for (int a : Ints.concat(intArr, intArr_3)) {
            System.out.println("intArr_4:" + a);
        }
//        boolean contains (prim[]array, prim target)判断原生类型数组是否包含给定值 Collection.contains 符号无关
        System.out.println(Ints.contains(intArr_3,3));
//        int indexOf (prim[]array, prim target)给定值在数组中首次出现处的索引，若不包含此值返回 - 1 List.indexOf 符号无关
        System.out.println(Ints.indexOf(intArr_3,3));
        int[] intArr_5 = {2, 3};
        int[] intArr_6 = {2, 4};
        System.out.println(Ints.indexOf(intArr_3,intArr_5));
        System.out.println(Ints.indexOf(intArr_3,intArr_6));
//        int lastIndexOf (prim[]array, prim target)给定值在数组最后出现的索引，若不包含此值返回 - 1 List.lastIndexOf 符号无关
        System.out.println(Ints.lastIndexOf(intArr_3,3));
//        prim min (prim…array)数组中最小的值 Collections.min 符号相关*
        System.out.println(Ints.min(intArr_3));
//        prim max (prim…array)数组中最大的值 Collections.max 符号相关
        System.out.println(Ints.max(intArr_3));
//        String join (String separator, prim…array)把数组用给定分隔符连接为字符串 Joiner.on(separator).join 符号相关
        System.out.println(Ints.join("#",intArr_3));

//        Comparator<prim[]> lexicographicalComparator () 按字典序比较原生类型数组的Comparator Ordering.natural().lexicographical()
        Comparator<int[]> comparator =  Ints.lexicographicalComparator();
//        符号相关
    }
}
